# testing_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

# Test coverage

[![pipeline status](https://gitlab.com/lgalvani1/testing_app/badges/main/pipeline.svg)](https://gitlab.com/lgalvani1/testing_app/-/commits/main)
[![coverage report](https://gitlab.com/lgalvani1/testing_app/badges/main/coverage.svg)](https://gitlab.com/lgalvani1/testing_app/-/commits/main)

[![pipeline status](https://gitlab.com/lgalvani1/testing_app/badges/feature/0002/pipeline.svg)](https://gitlab.com/lgalvani1/testing_app/-/commits/feature/0002)
[![coverage report](https://gitlab.com/lgalvani1/testing_app/badges/feature/0002/coverage.svg)](https://gitlab.com/lgalvani1/testing_app/-/commits/feature/0002)
